package com.ag04smarts.scc.converters;

import com.ag04smarts.scc.commands.CandidateCommand;
import com.ag04smarts.scc.models.Candidate;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CandidateToCandidateCommand implements Converter<Candidate, CandidateCommand> {


    @Synchronized
    @Nullable
    @Override
    public CandidateCommand convert(Candidate source){
        if(source == null){
            return null;
        }

        final CandidateCommand command = new CandidateCommand();
        command.setFirstName(source.getFirstName());
        command.setLastName(source.getLastName());
        command.setAge(source.getAge());
        command.setEmail(source.getEmail());
        command.setPhoneNumber(source.getPhoneNumber());
        command.setGender(source.getGender());
        command.setImage(source.getImage());
        return command;
    }
}
