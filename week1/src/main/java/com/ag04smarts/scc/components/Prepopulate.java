package com.ag04smarts.scc.components;

import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.models.Course;
import com.ag04smarts.scc.models.Lecturer;
import com.ag04smarts.scc.models.Registration;
import com.ag04smarts.scc.repositories.CandidateRepository;
import com.ag04smarts.scc.repositories.CourseRepoository;
import com.ag04smarts.scc.repositories.LecturerRepository;
import com.ag04smarts.scc.repositories.RegistrationRepository;
import com.ag04smarts.scc.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static com.ag04smarts.scc.models.CourseType.*;
import static com.ag04smarts.scc.models.Gender.FEMALE;
import static com.ag04smarts.scc.models.Gender.MALE;

/*
 * god class
 * */

@Component
@ConditionalOnProperty(
        value="init.data.enabled",
        havingValue = "true",
        matchIfMissing = true)
public class Prepopulate implements ApplicationListener<ContextRefreshedEvent> {

    private LecturerRepository lecturerRepository;
    private CourseRepoository courseRepository;
    private CandidateRepository candidateRepository;
    private RegistrationRepository registrationRepository;

    @Autowired
    public Prepopulate(LecturerRepository lecturerRepository, CourseRepoository courseRepository, CandidateRepository candidateRepository, RegistrationRepository registrationRepository) {
        this.lecturerRepository = lecturerRepository;
        this.courseRepository = courseRepository;
        this.candidateRepository = candidateRepository;
        this.registrationRepository = registrationRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent){
        try {
            initData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void initData() throws ParseException {

        //lecturers
        Lecturer gordon = new Lecturer("Gordon", "Ramsay");
        Lecturer jamie = new Lecturer("Jamie", "Oliver");
        Lecturer julia = new Lecturer("Julia", "Child");

        //courses
        Course learn = new Course("Learn to cook", BASIC, 8);
        Course mexican = new Course("Mexican cooking", BASIC, 3);
        Course style = new Course("Cooking with style",  INTERMEDIATE,  5);
        Course oliverCocking = new Course("Jamie Oliver Cooking course",  ADVANCED,  3);

        //add courses to gordon
        gordon.getCourses().add(learn);
        gordon.getCourses().add(mexican);
        gordon.getCourses().add(style);

        //add gordon to gordons courses
        learn.getLecturers().add(gordon);
        mexican.getLecturers().add(gordon);
        style.getLecturers().add(gordon);

        //add jamie to courses
        jamie.getCourses().add(learn);
        jamie.getCourses().add(mexican);

        //add jamie to jamies courses
        learn.getLecturers().add(jamie);
        mexican.getLecturers().add(jamie);

        //add julia to courses
        julia.getCourses().add(learn);

        //add julia to julias courses
        learn.getLecturers().add(julia);

        lecturerRepository.save(gordon);
        lecturerRepository.save(jamie);
        lecturerRepository.save(julia);

        courseRepository.save(learn);
        courseRepository.save(mexican);
        courseRepository.save(style);
        courseRepository.save(oliverCocking);

    }
}

