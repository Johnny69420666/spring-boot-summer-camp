package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.models.Lecturer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LecturerRepository extends CrudRepository<Lecturer, Long> {
}
