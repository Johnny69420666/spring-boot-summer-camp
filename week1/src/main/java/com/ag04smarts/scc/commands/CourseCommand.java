package com.ag04smarts.scc.commands;

import com.ag04smarts.scc.models.CourseType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CourseCommand {
    private Long id;
    private String name;
    private CourseType courseType;
    private Integer numberOfStudents;
    private List<RegistrationCommand> registrationCommands;
    private List<LecturerCommand> lecturerCommands;
}
