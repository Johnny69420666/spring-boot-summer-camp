package com.ag04smarts.scc.commands;

import com.ag04smarts.scc.models.Gender;
import com.ag04smarts.scc.validations.PhoneNumberConstraint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CandidateCommand {
    private String firstName;
    private String lastName;
    private Integer age;
    private String email;
    @PhoneNumberConstraint
    private String phoneNumber;
    private Gender gender;
    private Byte[] image;
}
