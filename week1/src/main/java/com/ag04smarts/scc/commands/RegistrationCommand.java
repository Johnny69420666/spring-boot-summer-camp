package com.ag04smarts.scc.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Null;
import java.util.List;

//register Courses to a candidate

@Getter
@Setter
@NoArgsConstructor
public class RegistrationCommand {

    private Long candidateId;
    private Long courseId;
}
