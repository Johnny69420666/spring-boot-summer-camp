package com.ag04smarts.scc.models;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Lecturer extends Person{

    @ManyToMany(mappedBy = "lecturers")
    private List<Course> courses = new ArrayList<>();

    public Lecturer(){}

    public Lecturer(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
