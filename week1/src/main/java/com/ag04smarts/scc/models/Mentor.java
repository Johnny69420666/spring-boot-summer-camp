package com.ag04smarts.scc.models;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Mentor extends Person{

    @OneToMany
    @JoinColumn(name = "MENTOR_FK")
    private List<Candidate> candidates;

    public Mentor(){}

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }
}
