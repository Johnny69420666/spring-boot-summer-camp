package com.ag04smarts.scc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;

import java.util.Arrays;


@Controller
@ConditionalOnProperty(
        value="print.beans.enabled",
        havingValue = "true",
        matchIfMissing = true)
public class PrintBeans implements CommandLineRunner {

    private ApplicationContext applicationContext;

    @Autowired
    public PrintBeans(ApplicationContext applicationContext){
        this.applicationContext = applicationContext;
    }

    public static void main(String[] args){
        SpringApplication.run(PrintBeans.class, args);
    }

    @Override
    public void run(String... args){
        String[] beans = applicationContext.getBeanDefinitionNames();
        Arrays.sort(beans);
        for (String bean : beans) {
            System.out.println(bean);
        }

    }
}
