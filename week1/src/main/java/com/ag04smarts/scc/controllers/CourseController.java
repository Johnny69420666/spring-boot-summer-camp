package com.ag04smarts.scc.controllers;


import com.ag04smarts.scc.models.Course;
import com.ag04smarts.scc.services.CandidateService;
import com.ag04smarts.scc.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api")
public class CourseController {//todo optimise mapping namings

    private final CourseService courseService;
    private final CandidateService candidateService;

    @Autowired
    public CourseController(CourseService courseService, CandidateService candidateService) {
        this.courseService = courseService;
        this.candidateService = candidateService;
    }

    @GetMapping("/candidates/{id}/register")
    public String registerForm(@PathVariable Long id, Model model){
        if (candidateService.findById(id) != null) {
            Iterable<Course> courses = courseService.getCourses();
            List<Course> availableCourses = new ArrayList<Course>();
            for (Course c : courses){
                if (!candidateService.findById(id).getRegistration().getCourses().contains(c)){
                    availableCourses.add(c);
                }
            }
            model.addAttribute("courses", availableCourses);
            model.addAttribute("candidateId",id);
            return "register";
        }
        return null;
    }


    @PostMapping("/candidates/{candidateId}/register/{courseId}")
    public String registerCourse(@PathVariable Long candidateId, @PathVariable Long courseId){
        courseService.registerCourse(candidateService.findById(candidateId).getRegistration().getId(),courseId);
        return "redirect:/api/candidates/{candidateId}";
    }
}
