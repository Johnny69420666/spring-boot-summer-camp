package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.repositories.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ImageServiceImpl implements ImageService {

    private final CandidateRepository candidateRepository;
    private final CandidateService candidateService;

    @Autowired
    public ImageServiceImpl(CandidateRepository candidateRepository, CandidateService candidateService) {
        this.candidateRepository = candidateRepository;
        this.candidateService = candidateService;
    }

    @Override
    public void saveImageFile(Long id, MultipartFile file){
        try {

            Candidate candidate = candidateService.findById(id);
            Byte[] byteObjects = new Byte[file.getBytes().length];

            int i = 0;
            for (byte b : file.getBytes()){
                byteObjects[i++] = b;
            }

            candidate.setImage(byteObjects);
            candidateRepository.save(candidate);
        }catch (IOException e){
            System.out.println("Error occured" + e);
            e.printStackTrace();
        }
    }
}
