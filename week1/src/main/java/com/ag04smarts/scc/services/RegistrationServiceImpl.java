package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Registration;
import com.ag04smarts.scc.repositories.CandidateRepository;
import com.ag04smarts.scc.repositories.CourseRepoository;
import com.ag04smarts.scc.repositories.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RegistrationServiceImpl implements RegistrationService{

    private RegistrationRepository registrationRepository;

    @Autowired
    public RegistrationServiceImpl(RegistrationRepository registrationRepository) {
        this.registrationRepository = registrationRepository;
    }

    @Override
    public Iterable<Registration> getRegistrations(){
        return registrationRepository.findAll();
    }

    @Override
    public Registration findById(Long l){
        Optional<Registration> registrationOptional = registrationRepository.findById(l);
        if (!registrationOptional.isPresent()){
            throw new RuntimeException("Error finding registration");
        }

        return  registrationOptional.get();
    }

}
