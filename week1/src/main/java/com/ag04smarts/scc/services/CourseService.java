package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Course;

import java.util.Optional;

public interface CourseService {

    void registerCourse(Long registrationId, Long courseId);

    Course findById(Long l);

    Iterable<Course> getCourses();
}
