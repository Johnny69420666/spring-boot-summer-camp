package com.ag04smarts.scc.services;

import com.ag04smarts.scc.commands.CandidateCommand;
import com.ag04smarts.scc.models.Candidate;

public interface CandidateService {

    Iterable<Candidate> getCandidates();

    Candidate findById(Long l);

    CandidateCommand findCommandById(Long l);

    CandidateCommand saveCandidateCommand(CandidateCommand command);
}
