package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Course;
import com.ag04smarts.scc.models.Registration;
import com.ag04smarts.scc.repositories.CourseRepoository;
import com.ag04smarts.scc.repositories.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService{

    private final CourseRepoository courseRepoository;
    private final RegistrationService registrationService;
    private final RegistrationRepository registrationRepository;

    @Autowired
    public CourseServiceImpl(CourseRepoository courseRepoository, RegistrationService registrationService, RegistrationRepository registrationRepository) {
        this.courseRepoository = courseRepoository;
        this.registrationService = registrationService;
        this.registrationRepository = registrationRepository;
    }

    @Override
    @Transactional
    public void registerCourse(Long registrationId, Long courseId){
        Registration registration = registrationService.findById(registrationId);
        Course course = findById(courseId);
        if (!registration.getCourses().contains(course)){
            System.out.println("Adding course to candidate");
            course.getRegistrations().add(registration);
            registration.getCourses().add(course);
            course.setNumberOfStudents(course.getNumberOfStudents()-1);
            courseRepoository.save(course);
            registrationRepository.save(registration);
        }else{
            throw new RuntimeException("Candidate already registered to the course");
        }
    }

    @Override
    public Course findById(Long l){
        Optional<Course> courseOptional = courseRepoository.findById(l);
        if (!courseOptional.isPresent()){
            throw new RuntimeException("Error while finding course");
        }
        return courseOptional.get();
    }

    @Override
    public Iterable<Course> getCourses(){
        return courseRepoository.findAll();
    }
}
